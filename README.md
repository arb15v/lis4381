## Alexander Barlow

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create BitBucket repo
    - Compile BitBucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create tables with query results locally
    - Create Users
    - Grant Users appreopriate permissions
    - Forward Engineer to CCI Server

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create tables and inserts 
    - Apply code into Oracle and gather results
    - Answer query questions

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Install something
    - Install something

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Install something
    - Install something

6. [P1 README.md](P1/README.md "My P1 README.md file")
    - Install something
    - Install something

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Install something
    - Install something

