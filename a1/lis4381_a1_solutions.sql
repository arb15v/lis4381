use arb15v;

-- 1)Backward-engineer the following query resultset: a.list (current) job title each employee has, b.includename, c.address, d.phone, e.SSN, f.order by last name in descending order, g.use old-style join.

select emp_id, emp_fname, emp_lname, 
CONCAT(emp_street, ', ', emp_city, ', ', emp_state, ' ', substring(emp_zip,1,5), '-', substring(emp_zip,6,4)) as address,
CONCAT('(', substring(emp_phone,1,3), ')', substring(emp_phone,4,3), '-', substring(emp_phone,7,4)) as phone_num,
CONCAT(substring(emp_ssn,1,3), '-', substring(emp_ssn,4,2), '-', substring(emp_ssn,6,4)) as emp_ssn, job_title
from employee e, job j
where j.job_id=e.job_id
order by emp_lname desc;

-- 2)List all job titles and salaries each employee HAS and HAD, include employee ID, full name, job ID, job title, salaries, and respective dates, sort by employee id and date, use old-style join.

select emp_id, emp_fname, emp_lname, eht_date, eht_job_id, job_title, eht_emp_salary_eht_notes
from employee e, emp_hist eh, job j
where e.emp_id=eh.emp_id
 and j.job_id=e.job_id
order by emp_id, eht_date;

-- 3)List employee and dependent full names, DOBs, relationships, and ages of both employee and respective dependent(s), sort by employee last name in ascending order, use naturaljoin:

select emp_fname, emp_lname, emp_dob,
TIMESTAMPDIFF(year, emp_dob, curdate()) as emp_age, 
dep_fname, dep_lname, dep_relation, dep_dob, 
TIMESTAMPDIFF(year, dep_dob, curdate()) as dep_age
from employee
 natural join dependant
order by emp_lname;

--4)  Create a transaction that updates job ID 1 to the following title“owner,” w/o the quotation marks, display the job records before and after the change, inside the transaction:

BEGIN TRANSACTION;
    select * from job;
    
    UPDATE job
    SET job_title='owner'
    WHERE job_id=1;

    select * from job;
COMMIT;

-- 5)Create a stored procedure that adds one record to the benefittable with the following values: benefitname“new benefit,” benefitnotes “testing,” both attribute values w/o the quotation marks, display the benefitrecords before and after the change, inside the stored procedure:

DROP PROCEDURE IF EXISTS insert_benefit;
DELIMITER //
create procedure insert_benefit()
BEGIN
 select * from benefit;

 insert into benefit
 (ben_name, ben_notes)
 VALUES
 ('new benefit','testing');

 select * from benefit;
END //
DELIMITER ;

CALL insert_benefit();
DROP PROCEDURE IF EXISTS insert_benefit;

-- 6)List employees’and dependents’ names and social security numbers, also include employees’ e-mail addresses, dependents’ mailing addresses, and dependents’ phone numbers. *MUST* display *ALL* employee data, even where there are no associated dependent values. (Major table: all rows displayed, minor table: display null values.



-- 7)Create “after insert on employee” trigger that automatically creates an audit record in the emp_hist table.



/*
1. C
2. B
3. B
4. C
5. D
6. C
7. D
8. B
9. C
10. C
11. D
12. D
13. C
14. A
15. A
16. C
17. A
18. B
19. C
20. D
*/
