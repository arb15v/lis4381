# LIS4381

## Alexander Barlow

### Assignment 2 Requirements:

1. Tables and Insert Statements
2. Include Indexes and Foreign Key SQL
3. Query Results sets
4. Five records per table
5. Questions


#### README.md should include the following item(s):

* Screenshot of Tables and Queries
* Screenshot of Table generation and Value insertion code
* Screenshot of user creation and Permission granting


#### Assignment Screenshots:

*Screenshot of Tables and Queries Results*:

![A2 Tables and Queries Screenshot](img/tables.png)

*Screenshot of Company Table and Values*:

![A2 Company Table Screenshot](img/company.png)

*Screenshot of Customer Table*:

![A2 Customer Table Screenshot](img/cus_create.png)

*Screenshot of Customer Values*:

![A2 Customer Values Screenshot](img/cus_values.png)

*Screenshot of User Creation and Privledges*:

![A2 User Creation and Privledges](img/users.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket Station Locations")
