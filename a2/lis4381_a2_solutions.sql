drop database if exists arb15v;
create database if not exists arb15v;
use arb15v;

-- -----------------------------------------------------------------------
-- table company
-- -----------------------------------------------------------------------
drop table if exists company;
create table if not exists company
(
    cmp_id INT unsigned not null auto_increment,
    cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
    cmp_street varchar(30) not null,
    cmp_city varchar(30) not null,
    cmp_state char(2) not null,
    cmp_zip int(9) unsigned zerofill not null comment 'no dashes',
    cmp_phone bigint unsigned not null comment 'ssn and zip codes can be zero-filled, but  not us area codes',
    cmp_ytd_sales decimal(10,2) unsigned not null comment '12,345,678.90',
    cmp_email varchar(100) null,
    cmp_url varchar(100) null,
    cmp_notes varchar(255) null,
    primary key (cmp_id)
)
Engine = innoDb CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

insert into company
VALUES
(null,'C-Corp','507 - 20th Ave. E. Apt. 2A','Seattle','WA','081226749','2065559867','12345678.00',null,'http://technologies.ci.fsu.edu/node/72','company notes1'),
(null,'S-Corp','908 W. Capital Way','Tacoma','WA','081226748','2065559482','9945678.00',null,'http://www.qctir.com','company notes2'),
(null,'Non-Profit-Corp','1234 Creek Edge Drive','Tampa','FL','081236749','2065559864','12345679.00',null,'http://www.alexbarlow.com','company notes3'),
(null,'LLC','231 Main Street','Jacksonville','FL','081216749','2065529867','12345675.00',null,'http://www.bored.com','company notes4'),
(null,'Partnership','123 Idk Street','Albany','NY','081221149','2065554567','13345678.00',null,'http://www.runescape.com','company notes5');

SHOW WARNINGS;

-- -----------------------------------------------------------------------
-- table customer
-- -----------------------------------------------------------------------
drop table if exists customer;
create table if not exists customer
(
    cus_id int unsigned not null auto_increment,
    cmp_id int unsigned not null,
    cus_ssn binary(64) not null,
    cus_salt binary(64) not null comment '"only" demo  pruposes - do NOT use salt in name!',
    cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
    cus_first varchar(15) not null,
    cus_last varchar(30) not null,
    cus_street varchar(30) null,
    cus_city varchar(30) null,
    cus_state char(2) null,
    cus_zip int(9) unsigned zerofill null,
    cus_phone bigint unsigned not null,
    cus_email varchar(100)  null,
    cus_balance decimal(7,2) unsigned not null,
    cus_tot_sales decimal(7,2) unsigned null,
    cus_notes varchar(255) null,
    primary key (cus_id),

    unique index ux_cus_ssn (cus_ssn ASC),
    index idx_cmp_id (cmp_id asc),

    CONSTRAINT fk_customer_company
     FOREIGN KEY (cmp_id)
     REFERENCES company(cmp_id)
     ON DELETE NO ACTION
     ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

-- salting and hashing sensitive data
set @salt=RANDOM_BYTES(64);

insert into customer
VALUES
(null,2,unhex(SHA2(CONCAT(@salt, 000456789),512)),@salt,'Discount','Wilbur','Denaway','23 Billings Gate','El Paso','TX','085703412','2145559857','test1@gmail.com','8319.87','37642.00','customer notes1'),
(null,4,unhex(SHA2(CONCAT(@salt, 001456789),512)),@salt,'Impulse','John','Storer','123 Main Street','Austin','TX','033303412','2144459857','test2@gmail.com','1329.00','37641.00','customer notes2'),
(null,3,unhex(SHA2(CONCAT(@salt, 002456789),512)),@salt,'Need-Based','Alex','Barlow','198 Lemmon Lane','Ellijay','GA','015703412','2142259857','test3@gmail.com','8312.00','37632.00','customer notes3'),
(null,1,unhex(SHA2(CONCAT(@salt, 003456789),512)),@salt,'Loyal','Ron','Murphy','657 West Street','Tampa','FL','075803412','2145559855','test4@gmail.com','8334.87','37649.00','customer notes4'),
(null,5,unhex(SHA2(CONCAT(@salt, 004456789),512)),@salt,'Wandering','Mystir','President','34 Capital Way','Atlanta','GA','085453412','2148769857','test5@gmail.com','4519.87','37624.00','customer notes5');

SHOW WARNINGS;

select * from company;
select * from customer;

-- Time to create some users! Woo!

-- 1.Limit user3 to select, update, and delete privileges on company and customer tables
-- 2.Limit user4 to select, and insert privileges on customer table


-- -----------------------------------------------------------------------
-- 1. create users
-- -----------------------------------------------------------------------
-- user 3
create user 'user3'@'localhost' IDENTIFIED BY 'test3';

--user 4
create user 'user4'@'localhost' IDENTIFIED BY 'test4';


-- -----------------------------------------------------------------------
-- 2. assign permissions
-- -----------------------------------------------------------------------
-- user 3
grant select, update, delete
 on arb15v.*
 to 'user3'@"localhost";
  show warnings;
-- this allows for all tables. If only specific tables, have to make grant statements for each table

-- user 4
grant select, insert
 on arb15v.customer
 to 'user4'@"localhost";
  show warnings;

  FLUSH PRIVLEDGES;

-- 3. show grants;
-- 4. select user(), version();
-- 5. show tables;
-- 6. describe *;
-- 7. select * from *;
-- 8. update company set cmp_id=6 where cmp_id=1;
-- 9. delete from company where cmp_id=6;
-- 10. insert into company
        -- VALUES
        -- (value things);
     -- insert into customer
        -- VALUES
        -- (value things);
-- 11. select * from company;
     -- insert into customer
        -- VALUES
        -- (value things);
-- 12. drop table if exists company;
    -- drop table if exists customer;

/* Questions
1. b
2. b
3. b
4. d
5. d
6. c
7. b
8. c
9. c
10. d
11. a
12. c
13. c
14. b
15. a
16. a
17. a
18. a
19. a
20. a
21. c
22. b
23. a
24. b
25. d
26. b
*/