# LIS4381

## Alexander Barlow

### Assignment 3 Requirements:

1. Tables and Insert Statements
2. Include Indexes and Foreign Key SQL
3. Query Results sets
4. Five records per table
5. Questions


#### README.md should include the following item(s):

* Screenshots of SQL Code.
* Screenshot of populated tables in Oracle.


#### Assignment Screenshots:

*Screenshot of Customer Table Creation*:

![A3 CUstomer Table Creation Screenshot](img/customer.png)

*Screenshot of Commodity and Order Table Creation*:

![A3 Commodity and Order Table Creation Screenshot](img/com_order.png)

*Screenshot of Insert Statements*:

![A3 Insert Statement Screenshot](img/insert.png)

*Screenshot Oracle Query Results*:

![A3 Oracle Query Results Screenshot](img/results.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket Station Locations")
