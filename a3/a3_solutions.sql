SET DEFINE OFF

DROP SEQUENCE seq_cus_id;
create sequence seq_cus_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table customer CASCADE CONSTRAINTS PURGE;
CREATE TABLE customer
(
cus_id      number(3,0) not null, -- max value 999
cus_fname   varchar2(15) not null,
cus_lname   varchar2(30) not null,
cus_street  varchar2(30) not null,
cus_city    varchar2(30) not null,
cus_state   char(2) not null,
cus_zip     number(9) not null, -- equivalent to number(9,0)
cus_phone   number(10) not null,
cus_email   varchar2(100),
cus_balance number(7,2),
cus_notes   varchar2(255),
CONSTRAINT pk_customer PRIMARY KEY(cus_id)
);

DROP SEQUENCE seq_com_id;
create sequence seq_com_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table commodity CASCADE CONSTRAINTS PURGE;
CREATE TABLE commodity
(
com_id      number not null,
com_name    varchar2(20),
com_price   number(8,2) not null,
com_notes   varchar2(255),
CONSTRAINT pk_commodity PRIMARY KEY(com_id),
CONSTRAINT uq_com_name UNIQUE(com_name)
);

DROP SEQUENCE seq_ord_id;
create sequence seq_ord_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

-- demo purposes: Quoted identifiers can be reserved words, although this is not recommended.
drop table order_table CASCADE CONSTRAINTS PURGE;
CREATE TABLE order_table
(
ord_id          number(4,0) not null,
cus_id          number,
com_id          number,
ord_num_units   number(5,0) not null,
ord_total_units number(8,2) not null,
ord_notes       varchar2(255),
CONSTRAINT pk_order PRIMARY KEY(ord_id),
CONSTRAINT fk_order_customer
FOREIGN KEY(cus_id)
REFERENCES customer(cus_id),
CONSTRAINT fk_order_commodity
FOREIGN KEY(com_id)
REFERENCES commodity(com_id),
CONSTRAINT check_unit CHECK(ord_num_units > 0),
CONSTRAINT check_total CHECK(ord_total_units > 0)
);

-- Oracle NEXTVAL function used to retrieve next value in sequence
INSERT INTO customer VALUES (seq_cus_id.nextval,'Dave','Smith','123 Main Street','Detroit','MI',12345,1234567890,'smith@gmail.com',12345.67,'recently moved');
INSERT INTO customer VALUES (seq_cus_id.nextval,'John','Joeseph','456 South Street','Tampa','FL',23456,1234567891,'joeseph@gmail.com',54321.98,NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval,'Margaret','Stanley','321 North Street','Tampa','FL',34567,1234567892,'stanley@gmail.com',45678.09,NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval,'Bob','Billy','875 West Street','Atlanta','GA',45678,1234567893,'billy@gmail.com',12568.73,NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval,'Joe','Shmoe','765 East Street','Chicago','IL',56789,1234567894,'shmoe@gmail.com',76859.12,NULL);
commit;

-- Oracle does NOT autoincrement by default! DML statements  will only last for the session

INSERT INTO commodity VALUES (seq_com_id.nextval,'DVD Player',109.00,NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval,'Cereal',3.00,NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval,'Scrabble',1.89,'what a sale');
INSERT INTO commodity VALUES (seq_com_id.nextval,'Tuna Fish',5.00,NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval,'Tums',2.45,NULL);
commit;

INSERT INTO order_table VALUES (seq_ord_id.nextval,1,2,50,200,NULL);
INSERT INTO order_table VALUES (seq_ord_id.nextval,2,3,30,100,NULL);
INSERT INTO order_table VALUES (seq_ord_id.nextval,3,1,6,654,NULL);
INSERT INTO order_table VALUES (seq_ord_id.nextval,5,4,24,972,NULL);
INSERT INTO order_table VALUES (seq_ord_id.nextval,3,5,7,300,NULL);
INSERT INTO order_table VALUES (seq_ord_id.nextval,1,2,5,15,NULL);
INSERT INTO order_table VALUES (seq_ord_id.nextval,2,3,40,57,NULL);
INSERT INTO order_table VALUES (seq_ord_id.nextval,3,1,4,300,NULL);
INSERT INTO order_table VALUES (seq_ord_id.nextval,5,4,14,770,NULL);
INSERT INTO order_table VALUES (seq_ord_id.nextval,3,5,15,883,NULL);
commit;

select * from customer;
select * from commodity;
select * from order_table;

-- 1.

select * from product_component_version;

-- 2.

select * from V

-- 3.

select user from dual;

-- 4.

select to_char
    (sysdate, 'MM-DD-YYYY HH24:MI:SS') "NOW"
    from dual;

-- 5.

select to_char
    (sysdate, 'MM-DD-YYYY HH12:MI:SS' AM) "NOW"
    from dual;
    
-- 6.



-- 7.



-- 8.



-- 9.



-- 10.



-- 11.



-- 12.



-- 13.



-- 14.



-- 15.



-- 16.



-- 17.



-- 18.



-- 19.



-- 20.



-- 21.



-- 22.



-- 23.



-- 24.



-- 25.



/* Multiple choice questions
1. 
2. 
3. 
4. 
5. 
6. 
7. 
8. 
9. 
10. 
11. 
12. 
13. 
14. 
15. 
16. 
17. 
18. 
19. 
20. 
21. 
22. 
23. 
24. 
25. 